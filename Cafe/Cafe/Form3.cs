﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Cafe
{
    public partial class Form3 : Form
    {

        // monetary values
        public double topay;
        public double moneygiven;
        public double change;
        // contains date, time, and above variables to create record of a transaction
        string transaction;

        // used for disc tray control
        [DllImport("winmm.dll", EntryPoint = "mciSendStringA", CharSet = CharSet.Ansi)]
        protected static extern int mciSendString(string lpstrCommand,
        StringBuilder lpstrReturnString,
        int uReturnLength,
        IntPtr hwndCallback);

        public Form3()
        {
            InitializeComponent();
        }

        // till will only open if customer has paid
        private void refresh()
        {
            if (moneygiven >= topay)
            {
                btnTill.Enabled = true;
            }
            else
            {

                btnTill.Enabled = false;
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            label1.Text = topay.ToString("£0.00");
            lblChange.Text = ("0.00");
            btnReceipt.Enabled = false;
            btnTill.Enabled = false;
        }

        // following 12 buttons correspond to monetary values and exact / clear. updates change calculation

        private void note20_Click(object sender, EventArgs e)
        {
            moneygiven = moneygiven + 20.00;
            lblPay.Text = moneygiven.ToString("£0.00");
            change = moneygiven - topay;
            if (moneygiven > topay)
            {
                lblChange.Text = change.ToString("£0.00");
            }
            refresh();
        }

        private void note10_Click(object sender, EventArgs e)
        {
            moneygiven = moneygiven + 10.00;
            lblPay.Text = moneygiven.ToString("£0.00");
            change = moneygiven - topay;
            if (moneygiven > topay)
            {
                lblChange.Text = change.ToString("£0.00");
            }
            refresh();
        }

        private void note5_Click(object sender, EventArgs e)
        {
            moneygiven = moneygiven + 5.00;
            lblPay.Text = moneygiven.ToString("£0.00");
            change = moneygiven - topay;
            if (moneygiven > topay)
            {
                lblChange.Text = change.ToString("£0.00");
            }
            refresh();
        }

        private void coin2_Click(object sender, EventArgs e)
        {
            moneygiven = moneygiven + 2.00;
            lblPay.Text = moneygiven.ToString("£0.00");
            change = moneygiven - topay;
            if (moneygiven > topay)
            {
                lblChange.Text = change.ToString("£0.00");
            }
            refresh();
        }

        private void coin1_Click(object sender, EventArgs e)
        {
            moneygiven = moneygiven + 1.00;
            lblPay.Text = moneygiven.ToString("£0.00");
            change = moneygiven - topay;
            if (moneygiven > topay)
            {
                lblChange.Text = change.ToString("£0.00");
            }
            refresh();
        }

        private void coin50_Click(object sender, EventArgs e)
        {
            moneygiven = moneygiven + 0.50;
            lblPay.Text = moneygiven.ToString("£0.00");
            change = moneygiven - topay;
            if (moneygiven > topay)
            {
                lblChange.Text = change.ToString("£0.00");
            }
            refresh();
        }

        private void coin20_Click(object sender, EventArgs e)
        {
            moneygiven = moneygiven + 0.20;
            lblPay.Text = moneygiven.ToString("£0.00");
            change = moneygiven - topay;
            if (moneygiven > topay)
            {
                lblChange.Text = change.ToString("£0.00");
            }
            refresh();
        }

        private void coin10_Click(object sender, EventArgs e)
        {
            moneygiven = moneygiven + 0.10;
            lblPay.Text = moneygiven.ToString("£0.00");
            change = moneygiven - topay;
            if (moneygiven > topay)
            {
                lblChange.Text = change.ToString("£0.00");
            }
            refresh();
        }

        private void exact_Click(object sender, EventArgs e)
        {
            moneygiven = topay;
            lblPay.Text = moneygiven.ToString("£0.00");
            lblChange.Text = ("£0.00");
            refresh();
        }

        private void coin5_Click(object sender, EventArgs e)
        {
            moneygiven = moneygiven + 0.05;
            lblPay.Text = moneygiven.ToString("£0.00");
            change = moneygiven - topay;
            if (moneygiven > topay)
            {
                lblChange.Text = change.ToString("£0.00");
            }
            refresh();
        }

        private void coin1p_Click(object sender, EventArgs e)
        {
            moneygiven = moneygiven + 0.01;
            lblPay.Text = moneygiven.ToString("£0.00");
            change = moneygiven - topay;
            if (moneygiven > topay)
            {
                lblChange.Text = change.ToString("£0.00");
            }
            refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            moneygiven = 0.0;
            lblPay.Text = moneygiven.ToString("£0.00");
            lblChange.Text = ("£0.00");
            refresh();
        }

        // open till
        private void btnTill_Click(object sender, EventArgs e)
        {
            mciSendString("set CDAudio door open", null, 0, IntPtr.Zero);
            btnReceipt.Enabled = true;
        }

        // sends transaction details to receipt form
        private void btnReceipt_Click(object sender, EventArgs e)
        {
            Form4 form4 = new Form4();
            form4.owed = topay;
            form4.given = moneygiven;
            form4.change = change;
            form4.Show();
        }

        // close till, close form and update the transaction database
        private void btnClose_Click(object sender, EventArgs e)
        {
            mciSendString("set CDAudio door closed", null, 0, IntPtr.Zero);
            transaction = DateTime.Now.ToString() + " ORDER TOTAL: " + topay.ToString("£0.00") + " RECEIVED: " + moneygiven.ToString("£0.00") + " CHANGE: " + change.ToString("£0.00");
            File.AppendAllText(@"f:\Cafe\Cafe\Resources\transactions.txt", transaction + Environment.NewLine);
            this.Close();
        }


    }
}
