﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Cafe
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
            // reads txt file and displays contents line by line in list
            string[] lines = File.ReadAllLines(@"f:\Cafe\Cafe\Resources\transactions.txt");
            listBox1.Items.AddRange(lines);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
