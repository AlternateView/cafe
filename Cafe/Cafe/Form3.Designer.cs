﻿namespace Cafe
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.note5 = new System.Windows.Forms.Button();
            this.note10 = new System.Windows.Forms.Button();
            this.note20 = new System.Windows.Forms.Button();
            this.coin2 = new System.Windows.Forms.Button();
            this.coin1 = new System.Windows.Forms.Button();
            this.coin50 = new System.Windows.Forms.Button();
            this.coin20 = new System.Windows.Forms.Button();
            this.coin10 = new System.Windows.Forms.Button();
            this.exact = new System.Windows.Forms.Button();
            this.lblPay = new System.Windows.Forms.Label();
            this.coin1p = new System.Windows.Forms.Button();
            this.coin5 = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lblChange = new System.Windows.Forms.Label();
            this.btnTill = new System.Windows.Forms.Button();
            this.btnReceipt = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(163, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Amount owed:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Money given:";
            // 
            // note5
            // 
            this.note5.Location = new System.Drawing.Point(181, 75);
            this.note5.Name = "note5";
            this.note5.Size = new System.Drawing.Size(75, 23);
            this.note5.TabIndex = 3;
            this.note5.Text = "£5";
            this.note5.UseVisualStyleBackColor = true;
            this.note5.Click += new System.EventHandler(this.note5_Click);
            // 
            // note10
            // 
            this.note10.Location = new System.Drawing.Point(99, 75);
            this.note10.Name = "note10";
            this.note10.Size = new System.Drawing.Size(75, 23);
            this.note10.TabIndex = 4;
            this.note10.Text = "£10";
            this.note10.UseVisualStyleBackColor = true;
            this.note10.Click += new System.EventHandler(this.note10_Click);
            // 
            // note20
            // 
            this.note20.Location = new System.Drawing.Point(18, 75);
            this.note20.Name = "note20";
            this.note20.Size = new System.Drawing.Size(75, 23);
            this.note20.TabIndex = 5;
            this.note20.Text = "£20";
            this.note20.UseVisualStyleBackColor = true;
            this.note20.Click += new System.EventHandler(this.note20_Click);
            // 
            // coin2
            // 
            this.coin2.Location = new System.Drawing.Point(17, 105);
            this.coin2.Name = "coin2";
            this.coin2.Size = new System.Drawing.Size(75, 23);
            this.coin2.TabIndex = 6;
            this.coin2.Text = "£2";
            this.coin2.UseVisualStyleBackColor = true;
            this.coin2.Click += new System.EventHandler(this.coin2_Click);
            // 
            // coin1
            // 
            this.coin1.Location = new System.Drawing.Point(99, 105);
            this.coin1.Name = "coin1";
            this.coin1.Size = new System.Drawing.Size(75, 23);
            this.coin1.TabIndex = 7;
            this.coin1.Text = "£1";
            this.coin1.UseVisualStyleBackColor = true;
            this.coin1.Click += new System.EventHandler(this.coin1_Click);
            // 
            // coin50
            // 
            this.coin50.Location = new System.Drawing.Point(181, 105);
            this.coin50.Name = "coin50";
            this.coin50.Size = new System.Drawing.Size(75, 23);
            this.coin50.TabIndex = 8;
            this.coin50.Text = "50p";
            this.coin50.UseVisualStyleBackColor = true;
            this.coin50.Click += new System.EventHandler(this.coin50_Click);
            // 
            // coin20
            // 
            this.coin20.Location = new System.Drawing.Point(18, 134);
            this.coin20.Name = "coin20";
            this.coin20.Size = new System.Drawing.Size(75, 23);
            this.coin20.TabIndex = 9;
            this.coin20.Text = "20p";
            this.coin20.UseVisualStyleBackColor = true;
            this.coin20.Click += new System.EventHandler(this.coin20_Click);
            // 
            // coin10
            // 
            this.coin10.Location = new System.Drawing.Point(99, 134);
            this.coin10.Name = "coin10";
            this.coin10.Size = new System.Drawing.Size(75, 23);
            this.coin10.TabIndex = 10;
            this.coin10.Text = "10p";
            this.coin10.UseVisualStyleBackColor = true;
            this.coin10.Click += new System.EventHandler(this.coin10_Click);
            // 
            // exact
            // 
            this.exact.BackColor = System.Drawing.Color.Lime;
            this.exact.Location = new System.Drawing.Point(181, 164);
            this.exact.Name = "exact";
            this.exact.Size = new System.Drawing.Size(75, 23);
            this.exact.TabIndex = 11;
            this.exact.Text = "Exact";
            this.exact.UseVisualStyleBackColor = false;
            this.exact.Click += new System.EventHandler(this.exact_Click);
            // 
            // lblPay
            // 
            this.lblPay.AutoSize = true;
            this.lblPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPay.Location = new System.Drawing.Point(163, 37);
            this.lblPay.Name = "lblPay";
            this.lblPay.Size = new System.Drawing.Size(60, 24);
            this.lblPay.TabIndex = 12;
            this.lblPay.Text = "£0.00";
            // 
            // coin1p
            // 
            this.coin1p.Location = new System.Drawing.Point(18, 164);
            this.coin1p.Name = "coin1p";
            this.coin1p.Size = new System.Drawing.Size(75, 23);
            this.coin1p.TabIndex = 13;
            this.coin1p.Text = "1p";
            this.coin1p.UseVisualStyleBackColor = true;
            this.coin1p.Click += new System.EventHandler(this.coin1p_Click);
            // 
            // coin5
            // 
            this.coin5.Location = new System.Drawing.Point(181, 135);
            this.coin5.Name = "coin5";
            this.coin5.Size = new System.Drawing.Size(75, 23);
            this.coin5.TabIndex = 14;
            this.coin5.Text = "5p";
            this.coin5.UseVisualStyleBackColor = true;
            this.coin5.Click += new System.EventHandler(this.coin5_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.Red;
            this.btnClear.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnClear.Location = new System.Drawing.Point(100, 164);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 15;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.button3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 199);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(157, 24);
            this.label4.TabIndex = 16;
            this.label4.Text = "Change to give:";
            // 
            // lblChange
            // 
            this.lblChange.AutoSize = true;
            this.lblChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChange.Location = new System.Drawing.Point(176, 199);
            this.lblChange.Name = "lblChange";
            this.lblChange.Size = new System.Drawing.Size(66, 24);
            this.lblChange.TabIndex = 17;
            this.lblChange.Text = "label5";
            // 
            // btnTill
            // 
            this.btnTill.BackColor = System.Drawing.SystemColors.Control;
            this.btnTill.Location = new System.Drawing.Point(12, 226);
            this.btnTill.Name = "btnTill";
            this.btnTill.Size = new System.Drawing.Size(244, 40);
            this.btnTill.TabIndex = 18;
            this.btnTill.Text = "Open Till";
            this.btnTill.UseVisualStyleBackColor = false;
            this.btnTill.Click += new System.EventHandler(this.btnTill_Click);
            // 
            // btnReceipt
            // 
            this.btnReceipt.Location = new System.Drawing.Point(12, 272);
            this.btnReceipt.Name = "btnReceipt";
            this.btnReceipt.Size = new System.Drawing.Size(244, 40);
            this.btnReceipt.TabIndex = 19;
            this.btnReceipt.Text = "Print Receipt";
            this.btnReceipt.UseVisualStyleBackColor = true;
            this.btnReceipt.Click += new System.EventHandler(this.btnReceipt_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnClose.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnClose.Location = new System.Drawing.Point(12, 318);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 40);
            this.btnClose.TabIndex = 21;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 382);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnReceipt);
            this.Controls.Add(this.btnTill);
            this.Controls.Add(this.lblChange);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.coin5);
            this.Controls.Add(this.coin1p);
            this.Controls.Add(this.lblPay);
            this.Controls.Add(this.exact);
            this.Controls.Add(this.coin10);
            this.Controls.Add(this.coin20);
            this.Controls.Add(this.coin50);
            this.Controls.Add(this.coin1);
            this.Controls.Add(this.coin2);
            this.Controls.Add(this.note20);
            this.Controls.Add(this.note10);
            this.Controls.Add(this.note5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form3";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button note5;
        private System.Windows.Forms.Button note10;
        private System.Windows.Forms.Button note20;
        private System.Windows.Forms.Button coin2;
        private System.Windows.Forms.Button coin1;
        private System.Windows.Forms.Button coin50;
        private System.Windows.Forms.Button coin20;
        private System.Windows.Forms.Button coin10;
        private System.Windows.Forms.Button exact;
        private System.Windows.Forms.Label lblPay;
        private System.Windows.Forms.Button coin1p;
        private System.Windows.Forms.Button coin5;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblChange;
        private System.Windows.Forms.Button btnTill;
        private System.Windows.Forms.Button btnReceipt;
        private System.Windows.Forms.Button btnClose;
    }
}