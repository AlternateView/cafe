﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cafe
{

    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            btnAddNew.Enabled = false;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
            btnLogout.Visible = false;
            button14.Enabled = false;
        }

        // database related variables
        DatabaseConnection objConnect;
        string conString;

        DataSet ds;
        DataRow dRow;

        int MaxRows; // number of rows in table
        int inc = 0; // used to move between records

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                // connects to database project
                objConnect = new DatabaseConnection();
                conString = Properties.Settings.Default.CafeConnectionString;
                objConnect.connection_string = conString;
                objConnect.Sql = Properties.Settings.Default.SQL;
                ds = objConnect.GetConnection;
                MaxRows = ds.Tables[0].Rows.Count;
                NavigateRecords();
                labelUpdate();
                txtId.Enabled = false;
                txtName.Enabled = false;
                txtPrice.Enabled = false;
                txtType.Enabled = false;
                this.lstProducts.View = View.List;

            }
            catch (Exception err)
            {

                MessageBox.Show(err.Message);

            }
        }

        // moves between product entries in the catalogue and changes the displayed text
        private void NavigateRecords()
        {

            dRow = ds.Tables[0].Rows[inc];

            txtId.Text = dRow.ItemArray.GetValue(0).ToString();
            txtName.Text = dRow.ItemArray.GetValue(1).ToString();
            txtPrice.Text = dRow.ItemArray.GetValue(2).ToString();
            txtType.Text = dRow.ItemArray.GetValue(3).ToString();
            labelUpdate();

        }

        // shows current position of catalogue
        private void labelUpdate()
        {

            label5.Text = "Product " + (inc + 1) + " of " + MaxRows + ".";

        }

        // moves forward in catalogue
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (inc != MaxRows - 1)
            { // count for rows starts at 0
                inc++;
                NavigateRecords(); // refresh displayed products
            }
            else
            {
                MessageBox.Show("No more products to display.");
            }
        }

        // moves back in catalogue
        private void btnPrevious_Click(object sender, EventArgs e)
        {
            if (inc > 0)
            {

                inc--;
                NavigateRecords(); // refresh displayed products

            }
            else
            {

                MessageBox.Show("First product displayed.");

            }
        }

        // final catalogue entry
        private void btnLast_Click(object sender, EventArgs e)
        {
            if (inc != MaxRows - 1)
            {

                inc = MaxRows - 1;
                NavigateRecords();

            }
        }

        // first catalogue entry
        private void btnFirst_Click(object sender, EventArgs e)
        {
            if (inc != 0)
            {

                inc = 0;
                NavigateRecords();

            }
        }

        // create new product entry
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            txtId.Clear();
            txtName.Clear();
            txtPrice.Clear();
            txtType.Clear();
            btnAddNew.Enabled = false;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            txtId.Enabled = false;
        }

        // cancels new product entry
        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigateRecords();
            btnCancel.Enabled = false;
            btnSave.Enabled = false;
            btnAddNew.Enabled = true;
            txtId.Enabled = true;
        }

        // saves new product entry
        private void btnSave_Click(object sender, EventArgs e)
        {
            DataRow row = ds.Tables[0].NewRow(); // new datarow object to add data to database
            row[1] = txtName.Text;
            row[2] = txtPrice.Text;
            row[3] = txtType.Text;
            ds.Tables[0].Rows.Add(row);

            try
            {
                objConnect.UpdateDatabase(ds);

                MaxRows = MaxRows + 1;
                inc = MaxRows - 1;

                MessageBox.Show("Product added.");
            }
            catch (Exception err)
            {

                MessageBox.Show(err.Message);

            }

            btnCancel.Enabled = false;
            btnSave.Enabled = false;
            btnAddNew.Enabled = true;
            txtId.Enabled = true;
        }

        // updates existing product
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DataRow row = ds.Tables[0].Rows[inc];

            row[1] = txtName.Text;
            row[2] = txtPrice.Text;
            row[3] = txtType.Text;

            try
            {

                objConnect.UpdateDatabase(ds);

                MessageBox.Show("Product updated.");

            }
            catch (Exception err)
            {

                MessageBox.Show(err.Message);

            }
        }

        // deletes existing product
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                ds.Tables[0].Rows[inc].Delete();
                objConnect.UpdateDatabase(ds);
                MaxRows = ds.Tables[0].Rows.Count;
                inc--;
                NavigateRecords();
                MessageBox.Show("Product deleted.");
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        public double total = 0;

        // 12 product buttons for each individual product, updates list and total cost

        private void button1_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.20 - Toast");
            total = total + 1.2;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.20 - Crumpets");
            total = total + 1.2;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£2.20 - Bacon Sandwich");
            total = total + 2.2;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.30 - Sausage Sandwich");
            total = total + 1.3;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.20 - Teacake");
            total = total + 1.2;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.20 - Small Coffee");
            total = total + 1.2;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.40 - Regular Coffee");
            total = total + 1.4;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.60 - Large Coffee");
            total = total + 1.6;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.10 - Small Tea");
            total = total + 1.1;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.30 - Regular Tea");
            total = total + 1.3;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.00 - Water");
            total = total + 1.0;
            txtTotal.Text = total.ToString("£0.00");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Add("£1.10 - Orange Juice");
            total = total + 1.1;
            txtTotal.Text = total.ToString("£0.00");
        }

        // manager login
        private void button13_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.evtFrm += new ShowFrm(form2_evtFrm);
            form2.Show();
        }

        // if manager is logged in, allow access to catalogue editing
        void form2_evtFrm()
        {
            btnAddNew.Enabled = true;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
            btnLogout.Visible = true;
            txtName.Enabled = true;
            txtPrice.Enabled = true;
            txtType.Enabled = true;
            button14.Enabled = true;
        }

        // remove a product in the list and udate total accordingly
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lstProducts.SelectedItems.Count > 0)
            {
                foreach (ListViewItem eachItem in lstProducts.SelectedItems)
                {
                    string selected = null;
                    selected = lstProducts.SelectedItems[0].SubItems[0].Text;
                    string sub = selected.Substring(1, 4);
                    double remove = Convert.ToDouble(sub);
                    total = total - remove;
                    txtTotal.Text = total.ToString("£0.00");
                    lstProducts.Items.Remove(eachItem);
                }
            }
        }

        // cancel transaction
        private void btnNew_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Clear();
            total = 0.0;
            txtTotal.Text = total.ToString("£0.00");
        }


        // brings up payment window - form3
        private void btnPay_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.topay = total;
            form3.Show();
        }

        // manager logout button. only shown if logged in, disables manager controls
        private void btnLogout_Click(object sender, EventArgs e)
        {
            btnAddNew.Enabled = false;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
            btnLogout.Visible = false;
            txtName.Enabled = false;
            txtPrice.Enabled = false;
            txtType.Enabled = false;
            button14.Enabled = false;
        }

        // next customer
        private void btnNextCustomer_Click(object sender, EventArgs e)
        {
            lstProducts.Items.Clear();
            total = 0.0;
            txtTotal.Text = total.ToString("£0.00");
        }

        // displays form5 - transaction database
        private void button14_Click(object sender, EventArgs e)
        {
            Form5 form5 = new Form5();
            form5.Show();
        }


    }
}
