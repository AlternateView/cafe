﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Cafe
{
    public partial class Form4 : Form
    {

        // used to print receipt
        private PrintDocument printDocument1 = new PrintDocument();
        // monetary values
        public double owed;
        public double given;
        public double change;


        private void Form4_Load(object sender, EventArgs e)
        {

            label3.Text = DateTime.Now.ToString();
            label8.Text = owed.ToString("£0.00");
            label9.Text = given.ToString("£0.00");
            label10.Text = change.ToString("£0.00");
        }

        public Form4()
        {
            InitializeComponent();
            printDocument1.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
            // remove windows border, otherwise will be printed on receipt
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.BackColor = System.Drawing.Color.White;
        }

        Bitmap memoryImage;

        // takes a screenshot of current form to be printed as a receipt
        private void CaptureScreen()
        {
            Graphics myGraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
        }


        private void printDocument1_PrintPage(System.Object sender,
           System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(memoryImage, 0, 0);
        }

        void print()
        {
            printDocument1.Print();
        }

        // receipt prints and then closes upon clicking it; could not be done automatically. unsure as to why
        private void Form4_MouseClick(object sender, MouseEventArgs e)
        {
            CaptureScreen();
            print();
            this.Close();
        }

    }
}
