﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cafe
{
    public delegate void ShowFrm();
    public partial class Form2 : Form
    {
        public event ShowFrm evtFrm;
        public Form2()
        {
            InitializeComponent();
        }

        // check if username and password are correct
        private void button1_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "Ben") && (textBox2.Text == "cafe123"))
            {
                if (evtFrm != null)
                {
                    evtFrm();
                    this.Close();
                }
            }
            else if ((textBox1.Text == "ben") && (textBox2.Text == "cafe123"))
            {
                if (evtFrm != null)
                {
                    evtFrm();
                    this.Close();
                }
            }
            else MessageBox.Show("Incorrect username or password.");
        }

    }
}
